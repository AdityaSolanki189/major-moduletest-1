[Deployed Link](https://module-test-one-adisol.netlify.app/)

## Instructions:
1. The game is designed for two players, one is you as a player and the other opponent is your computer.
2. There will simultaneous turns given to you and your computer, as soon as you choose one option for yourself ie. stone/paper/scissor you opponent will also strike at the same time.
3. In any turn if you win over computer the score will be shown on the screen.
4. Different message has been displayed on the screen as per the win of the participants.
5. There is a rules section at the bottom right, so as soon as you click on it, a pop up will open and you need to write the basic rule of the game, and there is a close button to close the pop up.

#### NOTE: For you turn in the game, you will have to choose the option manually by clicking on the one of the option, but for your computer it will be decided by the logic you write which will randomly decided