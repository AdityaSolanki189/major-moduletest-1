let playerScore = 0;

let pattern = ["rock", "paper", "scissor"];

let customCss = [
    {
        paper: "20px solid #FF4370",
    },
    {
        rock: "20px solid #FFD600",
    },
    {
        scissor: "20px solid #CC00FF",
    }

]

$("#closeBtn").click(function() {
    $(".rules-popup").addClass("hideVisible");
});

$("#rulesBtn").click(function() {
    $(".rules-popup").removeClass("hideVisible");
});

function userLost(userChoice, computerChoice){
    // Player Score Remains the same

    $("#show-result").text("YOU LOST");
    
    let userCss = customCss.find((item) => item[userChoice]);
    let computerCss = customCss.find((item) => item[computerChoice]);
    $("#playerBtn").css({"border": userCss[userChoice]});
    $("#computerBtn").css({"border": computerCss[computerChoice]});

    $("#playerBtn").html(`<img src="./assets/${userChoice}.png" alt="${userChoice}">`);
    $("#computerBtn").html(`<img src="./assets/${computerChoice}.png" alt="${computerChoice}">`);

    $(".game-body").addClass("hideDisplay");
    $(".game-result").removeClass("hideDisplay");

    $("#computerBtn").addClass("won")
}

function userWon(userChoice, computerChoice){
    playerScore++;
    $("#score").text(playerScore);

    $("#show-result").text("YOU WON");

    let userCss = customCss.find((item) => item[userChoice]);
    let computerCss = customCss.find((item) => item[computerChoice]);
    $("#playerBtn").css({"border": userCss[userChoice]});
    $("#computerBtn").css({"border": computerCss[computerChoice]});

    $("#playerBtn").html(`<img src="./assets/${userChoice}.png" alt="${userChoice}">`);
    $("#computerBtn").html(`<img src="./assets/${computerChoice}.png" alt="${computerChoice}">`);
    
    $(".game-body").addClass("hideDisplay");
    $(".game-result").removeClass("hideDisplay");

    $("#playerBtn").addClass("won")
}

function play(choice){
    let playerChoice = choice;
    let computerChoice = pattern[Math.floor(Math.random() * pattern.length)];

    $("#playerBtn").removeClass("won");
    $("#computerBtn").removeClass("won");

    if(playerChoice === computerChoice){
        $("#blink").removeClass("hideVisible");
        $("#blink").addClass("blink");
        $(".game-body").addClass("non-clickable");
        setInterval(function(){
            // $("#blink").removeClass("blink");
            // $("#blink").addClass("hideVisible");
            $(".game-body").removeClass("non-clickable");
        }, 2000);
    } else if(playerChoice === "rock" && computerChoice === "paper"){
        userLost(playerChoice, computerChoice);
    } else if(playerChoice === "rock" && computerChoice === "scissor"){
        userWon(playerChoice, computerChoice);
    } else if(playerChoice === "paper" && computerChoice === "rock"){
        userWon(playerChoice, computerChoice);
    } else if(playerChoice === "paper" && computerChoice === "scissor"){
        userLost(playerChoice, computerChoice);
    } else if(playerChoice === "scissor" && computerChoice === "rock"){
        userLost(playerChoice, computerChoice);
    } else if(playerChoice === "scissor" && computerChoice === "paper"){
        userWon(playerChoice, computerChoice);
    }
}

$("#playAgain").click(function() {
    $("#blink").addClass("hideVisible");
    $(".game-result").addClass("hideDisplay");
    $(".game-body").removeClass("hideDisplay");
});